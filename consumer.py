import pika
import logging

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host = 'localhost'))
channel = connection.channel()
channel.queue_declare(queue = 'json_queue', auto_delete=True)

print('... waiting for json files ...')


def callback(ch, method, properties, body):
    print '------------- received -------------'
    print body


channel.basic_consume(callback,
                      queue = 'json_queue')
channel.start_consuming()