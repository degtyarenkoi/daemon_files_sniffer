import os
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
import pika
import logging


class MyEventHandler(FileSystemEventHandler):
    def __init__(self, observer, filename):
        self.observer = observer
        self.filename = filename

    def on_created(self, event):
        if not event.is_directory:
            if event.src_path.endswith('.json'):
                print 'e=', event.src_path

                json_name = event.src_path.split('/')[6]
                print json_name
                file = open(json_name, 'r')
                json_content = file.read()

                send_json(json_name, json_content)
            else:
                print'not a JSON was created'
#        self.observer.unschedule_all()
#        self.observer.stop()


def daemonize():
    pid = os.fork()
    if pid == 0:
        os.setsid()
        pid = os.fork()
        if pid == 0:
            print'DAEMON STARTED'
            main()
        else:
            os._exit(0)
    else:
         os._exit(0)

def main():
    filename = ''
    path = os.getcwd()

    observer = Observer()
    event_handler = MyEventHandler(observer, filename)

    observer.schedule(event_handler, path, recursive=False)
    observer.start()
    observer.join()

    return 0

def send_json(json_name, json_content):
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='json_queue', auto_delete=True)

    logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                        filename='logs.log', level=logging.DEBUG)
    try:
        channel.basic_publish(
            exchange='',
            routing_key='json_queue',
            body=json_content)

        logging.info('JSON ' + json_name +  'was delivered')
    except pika.exceptions.AMQPConnectionError:
        logging.warning("disconnected from RMQ")
    except pika.exceptions.ConnectionClosed:
        logging.error("Connection closed. JSON wasn't delivered")
    connection.close()




if __name__ == '__main__':
    daemonize()